/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9962820417009192, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.9654190398698128, 500, 1500, "getChildCheckInCheckOutByArea"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.999478759447485, 500, 1500, "getDashboardData"], "isController": false}, {"data": [1.0, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [1.0, 500, 1500, "findAllLevels"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (health_check_type)"], "isController": false}, {"data": [1.0, 500, 1500, "getAllEvents"], "isController": false}, {"data": [1.0, 500, 1500, "addOrUpdateUserDevice"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (Relation_Child)"], "isController": false}, {"data": [1.0, 500, 1500, "me"], "isController": false}, {"data": [1.0, 500, 1500, "getNotificationCount"], "isController": false}, {"data": [1.0, 500, 1500, "dismissChildCheckInByVPS"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (non_parent_relationship)"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (guardian_rejection_reason)"], "isController": false}, {"data": [1.0, 500, 1500, "addClassesToArea"], "isController": false}, {"data": [1.0, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [1.0, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [1.0, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [1.0, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.9998651927743327, 500, 1500, "getChildCheckInCheckOut"], "isController": false}, {"data": [1.0, 500, 1500, "getClassAttendanceSummaries"], "isController": false}, {"data": [0.9998255205304176, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (checkout_decline_reason)"], "isController": false}, {"data": [1.0, 500, 1500, "getAllCentreClasses"], "isController": false}, {"data": [1.0, 500, 1500, "getAllArea"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (checkin_decline_reason)"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 107048, 0, 0.0, 69.79566175921063, 2, 5012, 32.0, 130.0, 186.0, 324.0, 355.77594171879053, 1232.1656330699168, 414.0775889009874], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getChildCheckInCheckOutByArea", 2458, 0, 0.0, 301.3360455655001, 107, 839, 277.0, 473.0999999999999, 519.0499999999997, 617.8199999999997, 8.195846726329409, 44.05749534023767, 27.21277233351561], "isController": false}, {"data": ["findAllConfigByCategory", 2767, 0, 0.0, 9.956631731116726, 2, 228, 5.0, 16.0, 32.0, 97.28000000000065, 9.2519217716076, 8.464122889729264, 8.032316603095232], "isController": false}, {"data": ["getDashboardData", 3837, 0, 0.0, 173.8019285900443, 69, 583, 160.0, 254.0, 290.0, 377.0, 12.828614128526867, 111.36088963718292, 34.15117393980883], "isController": false}, {"data": ["getAllClassInfo", 2455, 0, 0.0, 44.411405295315724, 9, 330, 31.0, 88.0, 115.19999999999982, 192.44000000000005, 8.211800909820711, 9.069869950202369, 8.027356162822786], "isController": false}, {"data": ["findAllLevels", 3705, 0, 0.0, 34.07665317138998, 5, 295, 18.0, 81.0, 114.0, 193.82000000000016, 12.394206001405012, 7.8916233524570965, 10.433403880088983], "isController": false}, {"data": ["findAllConfigByCategory (health_check_type)", 3708, 0, 0.0, 33.74676375404525, 5, 337, 19.0, 77.0, 109.0, 195.54999999999927, 12.398352236250803, 12.216735748415097, 11.914041602022255], "isController": false}, {"data": ["getAllEvents", 4444, 0, 0.0, 41.33775877587738, 7, 409, 23.0, 96.0, 129.0, 222.7500000000009, 14.865461551841793, 7.360145514437294, 16.142962153953196], "isController": false}, {"data": ["addOrUpdateUserDevice", 8593, 0, 0.0, 66.45909461189345, 10, 497, 48.0, 139.0, 186.0, 273.0, 28.727793044885296, 13.858915785325523, 35.04880847086267], "isController": false}, {"data": ["getHomefeed", 309, 0, 0.0, 4392.530744336567, 3523, 5012, 4401.0, 4699.0, 4783.0, 4917.599999999999, 1.0303846769460598, 194.24864254008997, 3.1303971972453053], "isController": false}, {"data": ["findAllConfigByCategory (Relation_Child)", 3708, 0, 0.0, 33.032901833872735, 5, 377, 18.0, 77.0, 107.0, 188.0, 12.397315920921974, 9.90332463214275, 11.876725506273885], "isController": false}, {"data": ["me", 2762, 0, 0.0, 81.15640839971039, 24, 498, 63.0, 153.0, 192.0, 280.0, 9.254140406954344, 14.687806127726905, 21.094611843130256], "isController": false}, {"data": ["getNotificationCount", 4750, 0, 0.0, 46.10463157894727, 8, 404, 27.0, 102.0, 144.0, 242.48999999999978, 15.889050938624777, 8.254858495457405, 17.096396676846812], "isController": false}, {"data": ["dismissChildCheckInByVPS", 2453, 0, 0.0, 42.546269873624084, 6, 364, 24.0, 97.59999999999991, 138.29999999999973, 231.76000000000022, 8.220206359685132, 4.567673260410976, 6.277540403587669], "isController": false}, {"data": ["findAllConfigByCategory (non_parent_relationship)", 3708, 0, 0.0, 33.26510248112193, 5, 388, 18.0, 78.0, 110.0, 194.8199999999997, 12.395450989830916, 10.131828592273903, 11.983883281184188], "isController": false}, {"data": ["findAllConfigByCategory (guardian_rejection_reason)", 3707, 0, 0.0, 34.5673050984624, 5, 353, 19.0, 80.0, 113.0, 198.84000000000015, 12.39521578776662, 11.80208534479732, 12.007865294398913], "isController": false}, {"data": ["addClassesToArea", 2457, 0, 0.0, 80.49654049654048, 14, 433, 62.0, 158.20000000000027, 206.0, 298.6800000000003, 8.214973519499278, 4.500586078553804, 5.97671413283883], "isController": false}, {"data": ["getCountCheckInOutChildren", 3709, 0, 0.0, 40.340792666486955, 6, 390, 23.0, 93.0, 128.0, 227.0, 12.395686073986438, 7.783619282786407, 14.453563644863094], "isController": false}, {"data": ["getCentreHolidaysOfYear", 8286, 0, 0.0, 53.730147236302194, 10, 422, 35.0, 114.0, 156.0, 249.0, 27.693572591183912, 50.434730454741434, 27.18101950222759], "isController": false}, {"data": ["findAllChildrenByParent", 309, 0, 0.0, 90.16504854368931, 20, 473, 62.0, 200.0, 280.0, 370.7999999999995, 1.042721198623203, 2.240221325167038, 1.5976851178123104], "isController": false}, {"data": ["findAllSchoolConfig", 8596, 0, 0.0, 62.84399720800383, 11, 432, 47.0, 125.0, 166.0, 245.0, 28.728026201457123, 626.517852666934, 17.199556085405387], "isController": false}, {"data": ["getChildCheckInCheckOut", 3709, 0, 0.0, 75.01833378269086, 17, 691, 55.0, 149.0, 200.5, 292.0, 12.368156914530001, 6.2565481266860745, 36.34353921466872], "isController": false}, {"data": ["getClassAttendanceSummaries", 4447, 0, 0.0, 53.20013492241961, 7, 390, 32.0, 123.0, 171.59999999999945, 268.5599999999986, 14.864855830620199, 8.419547247812222, 16.301985447057113], "isController": false}, {"data": ["getLatestMobileVersion", 8597, 0, 0.0, 21.472955682214707, 8, 646, 15.0, 35.0, 51.0, 108.0, 28.670428472333384, 15.622674665089242, 16.841212036986086], "isController": false}, {"data": ["findAllConfigByCategory (checkout_decline_reason)", 3706, 0, 0.0, 34.24905558553702, 5, 284, 18.0, 82.0, 118.0, 200.64999999999918, 12.392327858327537, 8.556031050622627, 11.980863847406507], "isController": false}, {"data": ["getAllCentreClasses", 3705, 0, 0.0, 51.46828609986501, 9, 381, 35.0, 102.0, 141.0, 221.0, 12.393169540566307, 17.004300004390295, 14.063342779431688], "isController": false}, {"data": ["getAllArea", 2455, 0, 0.0, 58.246028513238386, 10, 359, 42.0, 117.0, 149.19999999999982, 249.44000000000005, 8.210702341137123, 15.226683345526755, 8.07439185305184], "isController": false}, {"data": ["findAllConfigByCategory (checkin_decline_reason)", 3708, 0, 0.0, 32.81229773462768, 5, 395, 18.0, 76.0, 109.0, 196.90999999999985, 12.396362663813854, 8.219853758525007, 11.972658861828029], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 107048, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
